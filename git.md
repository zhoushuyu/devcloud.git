# 新建Gitee仓库

git全局设置
```shell
git config --global user.name "zhoushuyu"
git config --global user.email "zhoushuyu215@163.com"
```

git地址
```shell
git@gitee.com:zhoushuyu/devcloud.git
```

初始化项目
```shell
cd /Users/zhoushuyu/GoProjects/Go8/DEVCLOUD
git init
git add .
git commit -m "新建DEVCLOUD项目,新建cmdb,mcenter,web这三个目录"
git remote add origin git@gitee.com:zhoushuyu/devcloud.git
git push -u origin master
```


