package auth

import (
	"fmt"
	"gitee.com/zhoushuyu/devcloud/mcenter/apps/token"
	"gitee.com/zhoushuyu/devcloud/mcenter/apps/user"
	"github.com/emicklei/go-restful/v3"
	"github.com/infraboard/mcube/app"

	"github.com/infraboard/mcube/http/label"
	"github.com/infraboard/mcube/http/response"
	"github.com/infraboard/mcube/logger"
	"github.com/infraboard/mcube/logger/zap"
)

/*
在protocol/http.go中NewHTTPService()这个函数中，需要实现r.Filter(cors.Filter) 函数入参的类型，这个类型就是 FilterFunction

// FilterFunction definitions must call ProcessFilter on the FilterChain to pass on the control and eventually call the RouteFunction
type FilterFunction func(*Request, *Response, *FilterChain)
*/

// 做一个对象实现 FilterFunction 这个类型的函数

type Auther struct {
	log logger.Logger
	tk  token.Service
}

func NewAuther() *Auther {
	return &Auther{
		log: zap.L().Named("auther.http"),
		tk:  app.GetInternalApp(token.AppName).(token.Service),
	}
}

func (a *Auther) GoRestfulAuthFunc(req *restful.Request, resp *restful.Response, next *restful.FilterChain) {
	// 查看 req中有哪些可以用于认证
	meta := req.SelectedRoute().Metadata() // 请求拦截
	a.log.Debugf("route meta %s ", meta)   // 类似于这样： route meta map[openapi.tags:[令牌管理]]， 这个map中key是openapi.tags， value是[令牌管理]

	isAuth, ok := meta[label.Auth]
	if ok && isAuth.(bool) { // 如果字典中该key存在，且isAuth == true
		// 获取token
		ak := token.GetTokenFromHTTPHeader(req.Request)
		a.log.Debugf("获取的token是 %s", ak)

		// 判断用户传入的token是否合法
		token, err := a.tk.ValidateToken(req.Request.Context(), token.NewValidateTokenRequest(ak))
		fmt.Println(token)
		if err != nil {
			response.Failed(resp.ResponseWriter, err)
			return
		}
		// 判断是否开启鉴权
		v, ok := meta[label.Allow]

		/* 这段代码对应的是user/api/primary.go中的 Metadata(label.Allow, []interface{}{user.TYPE_PRIMARY, user.TYPE_SUPPER}).
			考察的是：空接口以及接口断言的用法
		if ok {
			//[PRIMARY SUPPER]
			value, ok := v.([]interface{})
			fmt.Println(value, ok)
			for _, v := range value {
				ut := v.(user.TYPE)
				if token.UserType < ut {
					response.Failed(resp.ResponseWriter, fmt.Errorf("permission deny: %s, 需要的权限是: %s", token.UserType, ut))
				}
			}
		}*/
		if ok {
			fmt.Println(v)
			ut := v.(user.TYPE)
			//if !token.UserType.Equal(ut) {
			if token.UserType < ut {
				response.Failed(resp.ResponseWriter, fmt.Errorf("permission deny: %s, 需要的权限是: %s", token.UserType, ut))
			}
		}
	}

	// next flow
	next.ProcessFilter(req, resp)

	// 响应拦截
	a.log.Debugf("%s ", resp)
	//a.log.Debugf("%+v ", *resp)

	//bt, err := json.Marshal(*resp)
	//a.log.Debugf("%s ", bt)
	//a.log.Debug(err)
}

/*
func (bs *BlogSet) String() string {
	bt, _ := json.Marshal(bs)
	return string(bt)
}
*/
