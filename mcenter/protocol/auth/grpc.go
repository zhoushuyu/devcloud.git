package auth

import (
	"context"
	"fmt"
	"gitee.com/zhoushuyu/devcloud/mcenter/apps/service"
	"github.com/infraboard/mcube/app"
	"github.com/infraboard/mcube/logger"
	"github.com/infraboard/mcube/logger/zap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
)

// grpc认证拦截中间件

// GrpcAuthUnaryServerInterceptor returns a new unary server interceptor for auth.
// 需要将GrpcAuthUnaryServerInterceptor() 添加到protocol/grpc.go 的中间件中
func GrpcAuthUnaryServerInterceptor() grpc.UnaryServerInterceptor {
	svr := app.GetInternalApp(service.AppName).(service.MetaService)
	return newGrpcAuther(svr).Auth
}

func newGrpcAuther(svr service.MetaService) *grpcAuther {
	return &grpcAuther{
		log:     zap.L().Named("Grpc Auther"),
		service: svr,
	}
}

type grpcAuther struct {
	log     logger.Logger
	service service.MetaService
}

func (g *grpcAuther) Auth(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (resp interface{}, err error) {
	g.log.Infof("进入mcenter/protocol/auth/grpc.go 的Auth函数")

	// 从context中获取认证信息
	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return nil, fmt.Errorf("ctx is not an grpc incoming context")
	}

	clientId, clientSecret := g.GetClientCredentialsFromMeta(md)
	if err := g.validateServiceCredential(clientId, clientSecret); err != nil {
		return nil, err
	}

	resp, err = handler(ctx, req)
	g.log.Infof("离开mcenter/protocol/auth/grpc.go 的Auth函数")
	return resp, err
}

// 定义拿到clientid和client secret的函数
func (g *grpcAuther) GetClientCredentialsFromMeta(md metadata.MD) (clientId, clientSecret string) {
	cids := md.Get(service.ClientHeaderKey)
	sids := md.Get(service.ClientSecretKey)
	if len(cids) > 0 {
		clientId = cids[0]
	}
	if len(sids) > 0 {
		clientSecret = sids[0]
	}
	return
}

func (g *grpcAuther) validateServiceCredential(clientId, clientSecret string) error {
	if clientId == "" && clientSecret == "" {
		return status.Errorf(codes.Unauthenticated, "client_id or client_secret is \"\"")
	}

	vsReq := service.NewValidateCredentialRequest(clientId, clientSecret)
	_, err := g.service.ValidateCredential(context.Background(), vsReq)
	if err != nil {
		return status.Errorf(codes.Unauthenticated, "service auth error, %s", err)
	}

	return nil
}
