package impl

import (
	"gitee.com/zhoushuyu/devcloud/mcenter/apps/endpoint"
	"gitee.com/zhoushuyu/devcloud/mcenter/apps/permission"
	"gitee.com/zhoushuyu/devcloud/mcenter/apps/policy"
	"gitee.com/zhoushuyu/devcloud/mcenter/apps/role"
	"github.com/infraboard/mcube/app"
	"github.com/infraboard/mcube/logger"
	"github.com/infraboard/mcube/logger/zap"
	"google.golang.org/grpc"
)

// 实例类
var (
	// Service 服务实例
	svr = &service{}
)

type service struct {
	permission.UnimplementedRPCServer

	log      logger.Logger
	policy   policy.Service
	role     role.Service
	endpoint endpoint.Service
}

func (s *service) Config() error {
	s.policy = app.GetInternalApp(policy.AppName).(policy.Service)
	s.role = app.GetInternalApp(role.AppName).(role.Service)
	s.endpoint = app.GetInternalApp(endpoint.AppName).(endpoint.Service)
	s.log = zap.L().Named(s.Name())
	return nil
}

func (s *service) Name() string {
	return permission.AppName
}

func (s *service) Registry(server *grpc.Server) {
	permission.RegisterRPCServer(server, svr)
}

func init() {
	app.RegistryGrpcApp(svr)
	app.RegistryInternalApp(svr)
}
