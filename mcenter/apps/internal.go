package apps

import (
	_ "gitee.com/zhoushuyu/devcloud/mcenter/apps/endpoint/impl"
	_ "gitee.com/zhoushuyu/devcloud/mcenter/apps/permission/impl"
	_ "gitee.com/zhoushuyu/devcloud/mcenter/apps/policy/impl"
	_ "gitee.com/zhoushuyu/devcloud/mcenter/apps/role/impl"
	_ "gitee.com/zhoushuyu/devcloud/mcenter/apps/service/impl"
	// 注册所有内部服务模块, 无须对外暴露的服务, 用于内部依赖
	_ "gitee.com/zhoushuyu/devcloud/mcenter/apps/token/impl"
	_ "gitee.com/zhoushuyu/devcloud/mcenter/apps/user/impl"
)
