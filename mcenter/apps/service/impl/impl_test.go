package impl_test

import (
	"context"
	"gitee.com/zhoushuyu/devcloud/mcenter/apps/service"
	"gitee.com/zhoushuyu/devcloud/mcenter/test/tools"
	"github.com/infraboard/mcube/app"
	"testing"

	// 注册所有模块
	_ "gitee.com/zhoushuyu/devcloud/mcenter/apps"
)

var svr service.MetaService

func TestCreateService(t *testing.T) {
	s := service.NewCreateServiceRequest()
	s.Name = "maudit"
	s.Description = "审计中心"
	s.Owner = "admin"
	ins, err := svr.CreateService(context.Background(), s)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(ins)
}

func init() {
	//// 初始化日志模块
	//zap.DevelopmentSetup()
	//
	//err := conf.LoadConfigFromToml("/Users/zhoushuyu/GoProjects/Go8/DEVCLOUD/mcenter/etc/config.toml")
	//if err != nil {
	//	panic(err)
	//}
	//
	//// 初始化全局app
	//if err = app.InitAllApp(); err != nil {
	//	panic(err)
	//}
	tools.DevelopmentSetup() // 测试用例初始化

	svr = app.GetInternalApp(service.AppName).(service.MetaService) // 拿到InternalApp，并断言是service.MetaService类型

}
