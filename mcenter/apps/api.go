package apps

import (
	_ "gitee.com/zhoushuyu/devcloud/mcenter/apps/service/api"
	// 注册所有HTTP服务模块, 暴露给框架HTTP服务器加载
	//_ "gitee.com/zhoushuyu/devcloud/mcenter/apps/book/api"
	_ "gitee.com/zhoushuyu/devcloud/mcenter/apps/token/api"
	_ "gitee.com/zhoushuyu/devcloud/mcenter/apps/user/api"
)
