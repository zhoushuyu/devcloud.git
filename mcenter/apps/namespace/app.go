package namespace

const name = "namespace"

const DEFAULT_NAMESPACE = "default"

type Service interface {
	RPCServer
}
