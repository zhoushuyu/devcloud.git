package impl_test

import (
	"context"
	"gitee.com/zhoushuyu/devcloud/mcenter/apps/domain"
	"gitee.com/zhoushuyu/devcloud/mcenter/apps/role"
	"gitee.com/zhoushuyu/devcloud/mcenter/test/tools"
	"github.com/infraboard/mcube/app"
	"testing"
)

var svr role.Service
var ctx = context.Background()

func init() {
	tools.DevelopmentSetup()

	svr = app.GetInternalApp(role.AppName).(role.Service)
}

func TestCreateRole(t *testing.T) {
	req := role.NewCreateRoleRequest()
	req.CreateBy = "test"
	req.Domain = domain.DEFAULT_DOMAIN
	req.Type = role.RoleType_GLOBAL
	req.Name = "maudit_admin"
	req.Description = "审计中心管理员"
	req.Specs = []*role.Spec{
		{
			Desc:   "单元测试",
			Effect: role.EffectType_ALLOW,
			// maudit service id
			ServiceId:    "2a4e174e",
			ResourceName: "Book",
			LabelKey:     "action",
			LabelValues:  []string{"list", "get"},
		},
	}
	r, err := svr.CreateRole(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(r)
}

func TestQueryRole(t *testing.T) {
	req := role.NewQueryRoleRequest()
	req.WithPermission = true // 是否需要带着权限查出来
	set, err := svr.QueryRole(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(set)
}
