package impl

import (
	"gitee.com/zhoushuyu/devcloud/mcenter/apps/policy"
	"gitee.com/zhoushuyu/devcloud/mcenter/apps/role"
	"gitee.com/zhoushuyu/devcloud/mcenter/apps/user"
	"gitee.com/zhoushuyu/devcloud/mcenter/conf"
	"github.com/infraboard/mcube/app"
	"github.com/infraboard/mcube/logger"
	"github.com/infraboard/mcube/logger/zap"
	"go.mongodb.org/mongo-driver/mongo"
	"google.golang.org/grpc"
)

var (
	// Service 服务实例
	svr = &impl{}
)

type impl struct {
	col *mongo.Collection
	log logger.Logger
	policy.UnimplementedRPCServer

	user user.Service
	role role.Service
}

func (i *impl) Config() error {
	db, err := conf.C().Mongo.GetDB()
	if err != nil {
		return err
	}
	i.col = db.Collection(i.Name())
	i.log = zap.L().Named(i.Name())

	i.user = app.GetInternalApp(user.AppName).(user.Service)
	i.role = app.GetInternalApp(role.AppName).(role.Service)
	return nil
}

func (i *impl) Name() string {
	return policy.AppName
}

func (i *impl) Registry(server *grpc.Server) {
	policy.RegisterRPCServer(server, svr)
}

func init() {
	app.RegistryInternalApp(svr)
	app.RegistryGrpcApp(svr)
}
