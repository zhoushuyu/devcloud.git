package impl_test

import (
	"context"
	"gitee.com/zhoushuyu/devcloud/mcenter/apps/domain"
	"gitee.com/zhoushuyu/devcloud/mcenter/apps/namespace"
	"gitee.com/zhoushuyu/devcloud/mcenter/apps/policy"
	"gitee.com/zhoushuyu/devcloud/mcenter/test/tools"
	"github.com/infraboard/mcube/app"
	"testing"
)

var impl policy.Service

func init() {
	tools.DevelopmentSetup()
	impl = app.GetInternalApp(policy.AppName).(policy.Service)
}

func TestCreatePolicy(t *testing.T) {
	req := policy.NewCreatePolicyRequest()
	req.Username = "maudit_admin"
	req.RoleId = "cdblktuvvhfkore10320"
	req.Domain = domain.DEFAULT_DOMAIN
	req.Namespace = namespace.DEFAULT_NAMESPACE
	req.CreateBy = "admin"
	r, err := impl.CreatePolicy(context.Background(), req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(r)
}

// 注意这个测试用例查不出来数据
func TestQueryPolicy(t *testing.T) {
	req := policy.NewQueryPolicyRequest()
	req.Domain = domain.DEFAULT_DOMAIN
	req.Namespace = namespace.DEFAULT_NAMESPACE
	req.Username = "maudit_admin"
	//req.WithRole = true
	r, err := impl.QueryPolicy(context.Background(), req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(r)

	//req := policy.NewQueryPolicyRequest()
	////req.Domain = domain.DEFAULT_DOMAIN
	//req.Username = "maudit_admin"
	////req.WithRole = true
	//rs, err := impl.QueryPolicy(context.Background(), req)
	//if err != nil {
	//	t.Fatal(err)
	//}
	//t.Log(rs)
}
