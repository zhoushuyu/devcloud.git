package token

const (
	ACCESS_TOKEN_HEADER_KEY   = "Authorization"
	VALIDATE_TOKEN_HEADER_KEY = "X-VALIDATE-TOKEN"
)
