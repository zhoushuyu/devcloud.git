/*API 的 Restful接口*/

package api

import (
	"gitee.com/zhoushuyu/devcloud/mcenter/apps/token"
	"github.com/emicklei/go-restful/v3"
	"github.com/infraboard/mcube/http/response"
)

// 颁发token
func (h *handler) IssueToken(r *restful.Request, w *restful.Response) {
	h.log.Infof("执行了 token/api/token.go")
	req := token.NewIssueTokenRequest()

	if err := r.ReadEntity(req); err != nil {
		response.Failed(w.ResponseWriter, err)
		return
	}

	tk, err := h.service.IssueToken(r.Request.Context(), req)
	if err != nil {
		response.Failed(w.ResponseWriter, err)
		return
	}

	response.Success(w.ResponseWriter, tk)
}

// Refresh Token 必须传递, 判断撤销者身份

func (h *handler) RevolkToken(r *restful.Request, w *restful.Response) {
	h.log.Infof("执行了 token/api/token.go")
	qs := r.Request.URL.Query()
	req := token.NewRevolkTokenRequest()
	// 从HTTP Header中获取Access token
	req.AccessToken = token.GetTokenFromHTTPHeader(r.Request)
	// 从Query string获取Refresh Token
	req.RefreshToken = qs.Get("refresh_token")

	ins, err := h.service.RevolkToken(r.Request.Context(), req) // 调用的是impl/token.go 中的RevolkToken函数
	if err != nil {
		response.Failed(w.ResponseWriter, err)
		return
	}

	response.Success(w.ResponseWriter, ins)
}

func (h *handler) ChangeNamespace(r *restful.Request, w *restful.Response) {
	req := token.NewChangeNamespaceRequest()
	if err := r.ReadEntity(req); err != nil {
		response.Failed(w.ResponseWriter, err)
		return
	}

	set, err := h.service.ChangeNamespace(r.Request.Context(), req)
	if err != nil {
		response.Failed(w.ResponseWriter, err)
		return
	}
	response.Success(w.ResponseWriter, set)
}

func (u *handler) ValidateToken(r *restful.Request, w *restful.Response) {
	tk := r.Request.Header.Get(token.VALIDATE_TOKEN_HEADER_KEY)
	req := token.NewValidateTokenRequest(tk)

	resp, err := h.service.ValidateToken(r.Request.Context(), req)
	if err != nil {
		response.Failed(w.ResponseWriter, err)
		return
	}
	response.Success(w.ResponseWriter, resp)
}
