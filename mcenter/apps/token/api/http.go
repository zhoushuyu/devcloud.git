/*将这个API暴露出去*/

package api

import (
	"gitee.com/zhoushuyu/devcloud/mcenter/apps/token"
	restfulspec "github.com/emicklei/go-restful-openapi"
	"github.com/emicklei/go-restful/v3"
	"github.com/infraboard/mcube/app"
	"github.com/infraboard/mcube/http/response"
	"github.com/infraboard/mcube/logger"
	"github.com/infraboard/mcube/logger/zap"
)

var (
	h = &handler{}
)

type handler struct {
	service token.Service
	log     logger.Logger
}

func (h *handler) Config() error {
	h.log = zap.L().Named(token.AppName)
	h.service = app.GetGrpcApp(token.AppName).(token.Service)
	return nil
}

func (h *handler) Name() string {
	return token.AppName
}

func (h *handler) Version() string {
	return "v1"
}

func (h *handler) Registry(ws *restful.WebService) {
	tags := []string{"令牌管理"}

	ws.Route(ws.POST("/").To(h.IssueToken).
		Doc("颁发令牌(登录)").
		Metadata(restfulspec.KeyOpenAPITags, tags).
		Reads(token.IssueTokenRequest{}).
		Writes(response.NewData(token.Token{})))

	ws.Route(ws.DELETE("/").To(h.RevolkToken).
		Doc("撤销令牌(登出)").
		Metadata(restfulspec.KeyOpenAPITags, tags))

	ws.Route(ws.PATCH("/").To(h.ChangeNamespace).
		Doc("切换空间").
		Metadata(restfulspec.KeyOpenAPITags, tags).
		Reads(token.ChangeNamespaceRequest{}))

	ws.Route(ws.GET("/").To(h.ValidateToken).
		Doc("验证令牌").
		Metadata(restfulspec.KeyOpenAPITags, tags).
		Reads(token.ValidateTokenRequest{}).
		Writes(response.NewData(token.Token{})).
		Returns(200, "OK", token.Token{}))
}

func init() {
	app.RegistryRESTfulApp(h)
}

/*
2022-10-10T16:44:55.287+0800    INFO    [INIT]  cmd/start.go:153        log level: debug
panic: interface conversion: *impl.service is not token.Service: missing method ChangeNamespace

goroutine 1 [running]:
gitee.com/zhoushuyu/devcloud/mcenter/apps/token/api.(*handler).Config(0x216a7e0)
        /Users/zhoushuyu/GoProjects/Go8/DEVCLOUD/mcenter/apps/token/api/http.go:26 +0x9b
github.com/infraboard/mcube/app.InitAllApp()
        /Users/zhoushuyu/go/pkg/mod/github.com/infraboard/mcube@v1.9.0/app/all.go:18 +0x197
gitee.com/zhoushuyu/devcloud/mcenter/cmd.glob..func3(0x215e400, {0x1989c0c, 0x0, 0x0})
        /Users/zhoushuyu/GoProjects/Go8/DEVCLOUD/mcenter/cmd/start.go:41 +0x73
github.com/spf13/cobra.(*Command).execute(0x215e400, {0x219d8e0, 0x0, 0x0})
        /Users/zhoushuyu/go/pkg/mod/github.com/spf13/cobra@v1.5.0/command.go:872 +0x624
github.com/spf13/cobra.(*Command).ExecuteC(0x215e180)
        /Users/zhoushuyu/go/pkg/mod/github.com/spf13/cobra@v1.5.0/command.go:990 +0x3bc
github.com/spf13/cobra.(*Command).Execute(...)
        /Users/zhoushuyu/go/pkg/mod/github.com/spf13/cobra@v1.5.0/command.go:918
gitee.com/zhoushuyu/devcloud/mcenter/cmd.Execute()
        /Users/zhoushuyu/GoProjects/Go8/DEVCLOUD/mcenter/cmd/root.go:38 +0x25
main.main()
        /Users/zhoushuyu/GoProjects/Go8/DEVCLOUD/mcenter/main.go:8 +0x17
exit status 2
make: *** [run] Error 1


*/
