package impl

import (
	"gitee.com/zhoushuyu/devcloud/mcenter/apps/token"
	"gitee.com/zhoushuyu/devcloud/mcenter/apps/token/provider"
	"gitee.com/zhoushuyu/devcloud/mcenter/conf"
	"github.com/infraboard/mcube/app"
	"github.com/infraboard/mcube/logger"
	"github.com/infraboard/mcube/logger/zap"
	"go.mongodb.org/mongo-driver/mongo"
	"google.golang.org/grpc"

	// 加载所有的 provider
	_ "gitee.com/zhoushuyu/devcloud/mcenter/apps/token/provider/all"
)

var svr = &service{} // Service 服务实例

type service struct {
	col *mongo.Collection
	log logger.Logger
	token.UnimplementedRPCServer
}

func (s *service) Config() error {
	db, err := conf.C().Mongo.GetDB()
	if err != nil {
		return err
	}
	s.col = db.Collection(s.Name())
	s.log = zap.L().Named(s.Name())

	// 初始化化provider
	return provider.Init()
}

func (s *service) Name() string {
	return token.AppName
}

func (s *service) Registry(server *grpc.Server) {
	token.RegisterRPCServer(server, svr)
}

func init() {
	app.RegistryGrpcApp(svr)     // 注册给GRPC
	app.RegistryInternalApp(svr) // 注册给内部服务
}
