package user

import (
	"context"
	"fmt"
	"gitee.com/zhoushuyu/devcloud/mcenter/apps/domain"
	"github.com/go-playground/validator/v10"
	"github.com/infraboard/mcube/exception"
	"github.com/infraboard/mcube/http/request"
	"github.com/rs/xid"
	"golang.org/x/crypto/bcrypt"
	"net/http"
	"strings"
	"time"
)

const AppName = "user"

// 模块内部接口，进程内部调用接口

type Service interface {
	CreateUser(context.Context, *CreateUserRequest) (*User, error)
	UpdateUser(context.Context, *UpdateUserRequest) (*User, error)
	DeleteUser(context.Context, *DeleteUserRequest) (*User, error)
	UpdatePassword(context.Context, *UpdatePasswordRequest) (*Password, error)
	ResetPassword(context.Context, *ResetPasswordRequest) (*Password, error)
	//mustEmbedUnimplementedRPCServer()
	// RPC 接口
	RPCServer
}

var validate = validator.New()

func NewCreateUserRequest() *CreateUserRequest {
	return &CreateUserRequest{
		Domain: domain.DEFAULT_DOMAIN,
	}
}

func (req *CreateUserRequest) Validate() error {
	return validate.Struct(req)
}

// 创建User，返回User实例
func New(req *CreateUserRequest) (*User, error) {
	if err := req.Validate(); err != nil {
		return nil, exception.NewBadRequest(err.Error())
	}

	// 将密码加密后保存
	pass, err := NewHashedPassword(req.Password)
	if err != nil {
		return nil, exception.NewBadRequest(err.Error())
	}

	u := &User{
		Id:            xid.New().String(), // 产生随机ID
		CreateAt:      time.Now().UnixMilli(),
		Spec:          req,
		Password:      pass,
		Profile:       &Profile{},
		IsInitialized: false,
		Status: &Status{
			Locked: false,
		},
	}
	return u, nil
}

// NewHashedPassword 用户密码加密函数
// 使用bcrypt包 加密密码
func NewHashedPassword(password string) (*Password, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 10)
	if err != nil {
		return nil, err
	}

	return &Password{
		Password:      string(bytes),
		CreateAt:      time.Now().UnixMilli(), // 1665286541208
		UpdateAt:      time.Now().UnixMilli(),
		ExpiredDays:   90,
		ExpiredRemind: 30,
	}, nil
}

// 设置密码需要被重置
func (p *Password) SetNeedReset(format string, a ...interface{}) {
	p.NeedReset = true
	p.ResetReason = fmt.Sprintf(format, a...)
}

func NewUserSet() *UserSet {
	return &UserSet{
		Items: []*User{},
	}
}

func (u *UserSet) Add(item *User) {
	u.Items = append(u.Items, item)
}

func NewDefaultUser() *User {
	return &User{}
}

// Desensitize 脱敏函数
func (u *User) Desensitize() {
	if u.Password != nil {
		u.Password.Password = ""
		u.Password.History = []string{}
	}
}

func (req *QueryUserRequest) WithType(t TYPE) {
	req.Type = &t
}

func NewQueryUserRequestFromHTTP(r *http.Request) *QueryUserRequest {
	query := NewQueryUserRequest()
	qs := r.URL.Query()
	query.Page = request.NewPageRequestFromHTTP(r)
	query.Keywords = qs.Get("keywords")
	query.SkipItems = qs.Get("skip_items") == "true"

	uids := qs.Get("user_ids")
	if uids != "" {
		query.UserIds = strings.Split(uids, ",")
	}
	return query

}

// NewQueryUserRequest 列表查询请求
func NewQueryUserRequest() *QueryUserRequest {
	return &QueryUserRequest{
		Page:      request.NewPageRequest(20, 1), // 参数是pagesize 和 pagenumber
		SkipItems: false,
	}
}

func NewDescribeUserRequestWithId(id string) *DescribeUserRequest {
	return &DescribeUserRequest{
		DescribeBy: DESCRIBE_BY_USER_ID,
		Id:         id,
	}
}

// NewDescriptUserRequestWithId 查询详情请求
func NewDescriptUserRequestWithName(username string) *DescribeUserRequest {
	return &DescribeUserRequest{
		DescribeBy: DESCRIBE_BY_USER_NAME,
		Username:   username,
	}
}

// CheckPassword 判断password 是否正确, 是明文的
func (p *Password) CheckPassword(password string) error {
	err := bcrypt.CompareHashAndPassword([]byte(p.Password), []byte(password))
	if err != nil {
		return exception.NewUnauthorized("用户名获取密码错误")
	}

	return nil
}
