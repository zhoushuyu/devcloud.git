package api

import (
	"fmt"
	"gitee.com/zhoushuyu/devcloud/mcenter/apps/user"
	restfulspec "github.com/emicklei/go-restful-openapi"
	"github.com/emicklei/go-restful/v3"
	"github.com/infraboard/mcube/app"
	"github.com/infraboard/mcube/http/label"
	"github.com/infraboard/mcube/http/response"
	"github.com/infraboard/mcube/logger"
	"github.com/infraboard/mcube/logger/zap"
)

var (
	primaryHandler = &primary{}
)

type primary struct {
	service user.Service
	log     logger.Logger
}

func (h *primary) Config() error {
	h.log = zap.L().Named(user.AppName)
	h.service = app.GetGrpcApp(user.AppName).(user.Service)
	return nil
}

func (h *primary) Name() string {
	return "user/primary"
}

func (h *primary) Version() string {
	return "v1"
}

// Registry 需要提供的Restful接口
func (h *primary) Registry(ws *restful.WebService) {
	tags := []string{"主账号管理"}

	ws.Route(ws.POST("").To(h.CreateUser).
		Metadata(label.Resource, "子账号").
		Metadata(label.Auth, true).
		Metadata(label.Allow, user.TYPE_PRIMARY).
		Doc("创建子账号").
		Metadata(restfulspec.KeyOpenAPITags, tags).
		Reads(user.CreateUserRequest{}).
		Writes(user.User{}))
	// 暴露下面定义的QueryUser和DescribeUser 这两个接口
	//route meta map[allow:PRIMARY auth:%!s(bool=true) openapi.tags:[主账号管理] resource:子账号]
	ws.Route(ws.GET("/").To(h.QueryUser).
		Metadata(label.Resource, "子账号").
		Metadata(label.Auth, true).
		//Metadata(label.Allow, []interface{}{user.TYPE_PRIMARY, user.TYPE_SUPPER}).
		Metadata(label.Allow, user.TYPE_PRIMARY).
		Doc("查询子账号列表").
		Metadata(restfulspec.KeyOpenAPITags, tags).
		Reads(user.QueryUserRequest{}).
		Writes(response.NewData(user.User{})))
	ws.Route(ws.GET("/{id}").To(h.DescribeUser).
		Metadata(label.Resource, "子账号").
		Metadata(label.Auth, true).
		Doc("查询某一个子账号详情").
		Param(ws.PathParameter("id", "identifier of the user").DataType("integer").DefaultValue("1")).
		Metadata(restfulspec.KeyOpenAPITags, tags).
		Writes(response.NewData(user.User{})).
		Returns(200, "OK", response.NewData(user.User{})).
		Returns(404, "Not Found", nil))
}

// CreateUser 主账号创建子账号
func (h *primary) CreateUser(r *restful.Request, w *restful.Response) {
	// 通过http获取用户请求数据
	req := user.NewCreateUserRequest()

	if err := r.ReadEntity(req); err != nil {
		response.Failed(w.ResponseWriter, err)
		return
	}

	req.Type = user.TYPE_SUB

	set, err := h.service.CreateUser(r.Request.Context(), req)
	if err != nil {
		response.Failed(w.ResponseWriter, err)
		return
	}

	response.Success(w.ResponseWriter, set)
}

// QueryUser 查询子账号
func (h *primary) QueryUser(r *restful.Request, w *restful.Response) {
	req := user.NewQueryUserRequestFromHTTP(r.Request)
	//req.Type = user.TYPE_SUB  //这样的话会报错 Cannot use 'user.TYPE_SUB' (type TYPE) as the type *TYPE
	// 这样控制 查询的只能是子账户
	req.WithType(user.TYPE_SUB)
	ins, err := h.service.QueryUser(r.Request.Context(), req)
	if err != nil {
		response.Failed(w.ResponseWriter, err)
		return
	}
	response.Success(w.ResponseWriter, ins)
}

// DescribeUser 查询某一个子账号的详情
func (h *primary) DescribeUser(r *restful.Request, w *restful.Response) {
	req := user.NewDescribeUserRequestWithId(r.PathParameter("id")) // 从URL参数中获得ID
	ins, err := h.service.DescribeUser(r.Request.Context(), req)
	if err != nil {
		response.Failed(w.ResponseWriter, err)
		return
	}
	if !ins.Spec.Type.Equal(user.TYPE_SUB) {
		response.Failed(w.ResponseWriter, fmt.Errorf("not an sub account"))
		return
	}

	// 对敏感信息进行脱敏
	ins.Desensitize()

	response.Success(w.ResponseWriter, ins)
}
