package impl

import (
	"context"
	"gitee.com/zhoushuyu/devcloud/mcenter/apps/user"
	"gitee.com/zhoushuyu/devcloud/mcenter/conf"
	"github.com/infraboard/mcube/app"
	"github.com/infraboard/mcube/logger"
	"github.com/infraboard/mcube/logger/zap"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"
	"google.golang.org/grpc"
)

// service接口的实现类
var (
	// Service 服务实例
	svr = &impl{}
)

type impl struct {
	col *mongo.Collection
	log logger.Logger
	user.UnimplementedRPCServer
}

func (s *impl) Config() error {

	db, err := conf.C().Mongo.GetDB()
	if err != nil {
		return err
	}
	// mongodb中表的名称是 user
	s.col = db.Collection(s.Name())
	// 设置唯一建
	indexs := []mongo.IndexModel{
		{
			Keys: bsonx.Doc{{Key: "create_at", Value: bsonx.Int32(-1)}},
		},
		{
			Keys: bsonx.Doc{
				{Key: "spec.domain", Value: bsonx.Int32(-1)},
				{Key: "spec.username", Value: bsonx.Int32(-1)},
			},
			Options: options.Index().SetUnique(true),
		},
	}
	_, err = s.col.Indexes().CreateMany(context.Background(), indexs)
	if err != nil {
		return err
	}

	s.log = zap.L().Named(s.Name())
	return nil
}

func (s *impl) Name() string {
	return user.AppName
}

func (s *impl) Registry(server *grpc.Server) {
	user.RegisterRPCServer(server, svr)
}

func init() {
	app.RegistryGrpcApp(svr)
	app.RegistryInternalApp(svr)
}
