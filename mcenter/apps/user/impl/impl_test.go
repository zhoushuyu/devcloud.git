package impl_test

import (
	"context"
	"gitee.com/zhoushuyu/devcloud/mcenter/apps/domain"
	"gitee.com/zhoushuyu/devcloud/mcenter/apps/user"
	"gitee.com/zhoushuyu/devcloud/mcenter/test/tools"
	"github.com/infraboard/mcube/app"
	"testing"
)

var (
	impl user.Service
	ctx  = context.Background()
)

func TestCreateUser(t *testing.T) {
	req := user.NewCreateUserRequest()
	// 子账号: 审核管理员
	req.Domain = domain.DEFAULT_DOMAIN
	req.Username = "maudit_admin"
	req.Password = "123456"
	req.Type = user.TYPE_SUB
	r, err := impl.CreateUser(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(r)
}

func init() {
	tools.DevelopmentSetup()
	impl = app.GetInternalApp(user.AppName).(user.Service)
}
