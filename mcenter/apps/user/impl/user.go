package impl

import (
	"context"
	"gitee.com/zhoushuyu/devcloud/mcenter/apps/user"
	"github.com/infraboard/mcube/exception"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

// 创建账号
// 有可能是管理员创建主账号
// 有可能是主账号创建子账号

func (i *impl) CreateUser(ctx context.Context, req *user.CreateUserRequest) (*user.User, error) {
	u, err := user.New(req)
	if err != nil {
		return nil, err
	}
	// 判断是不是管理员创建的账号，如果是，则需要用户自己重置密码
	if req.CreateBy.IsIn(user.CREATE_BY_ADMIN) {
		u.Password.SetNeedReset("admin created user need reset when first login")
	}

	// 持久化
	if err := i.save(ctx, u); err != nil {
		return nil, err
	}

	// 为了安全，将密码对象清空
	u.Password = nil
	u.Spec.Password = "" // Spec其实就是 *CreateUserRequest 对象指针

	return u, nil
}

func (i *impl) QueryUser(ctx context.Context, req *user.QueryUserRequest) (*user.UserSet, error) {
	r := newQueryRequest(req)
	resp, err := i.col.Find(ctx, r.FindFilter(), r.FindOption()) // Find函数返回一个 *Cursor
	if err != nil {
		return nil, exception.NewInternalServerError("find user error, the error is %s ", err)
	}

	// 将数据读出，发到一个集合中
	set := user.NewUserSet()
	if !req.SkipItems { // 当req.SkipItems为假时，进入逻辑
		for resp.Next(ctx) {
			ins := user.NewDefaultUser()
			if err := resp.Decode(ins); err != nil {
				return nil, exception.NewInternalServerError("decode user error, the error is %s ", err)
			}

			// 防止数据中有敏感信息，进行脱敏
			ins.Desensitize()
			set.Add(ins)
		}
	}

	return set, nil
}

func (i *impl) DescribeUser(ctx context.Context, req *user.DescribeUserRequest) (*user.User, error) {
	filter := bson.M{}
	switch req.DescribeBy {
	case user.DESCRIBE_BY_USER_ID:
		filter["_id"] = req.Id
	case user.DESCRIBE_BY_USER_NAME:
		filter["spec.username"] = req.Username
	default:
		return nil, exception.NewBadRequest("unknow describe by %s", req.DescribeBy)
	}
	ins := user.NewDefaultUser()
	if err := i.col.FindOne(ctx, filter).Decode(ins); err != nil {
		if err == mongo.ErrNoDocuments { // 如果没有查到用户
			return nil, exception.NewNotFound("user %s not found", req) // ins.Spec.Username
		}
		return nil, exception.NewInternalServerError("user %s error, %s ", req, err)
	}
	return ins, nil
}

func (i *impl) UpdateUser(context.Context, *user.UpdateUserRequest) (*user.User, error) {
	return nil, nil
}

func (i *impl) DeleteUser(context.Context, *user.DeleteUserRequest) (*user.User, error) {
	return nil, nil
}

func (i *impl) UpdatePassword(context.Context, *user.UpdatePasswordRequest) (*user.Password, error) {
	return nil, nil
}

func (i *impl) ResetPassword(context.Context, *user.ResetPasswordRequest) (*user.Password, error) {
	return nil, nil
}
