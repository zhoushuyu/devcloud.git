package cmd

import (
	"context"
	"fmt"
	"gitee.com/zhoushuyu/devcloud/mcenter/apps/domain"
	"gitee.com/zhoushuyu/devcloud/mcenter/apps/user"
	"github.com/infraboard/mcube/app"
	"github.com/spf13/cobra"
)

var (
	createTableFilePath string
)

// initCmd represents the start command
var initCmd = &cobra.Command{
	Use:   "init",
	Short: "mcenter 服务初始化",
	Long:  "mcenter 服务初始化",
	RunE: func(cmd *cobra.Command, args []string) error {
		// 初始化全局变量
		if err := loadGlobalConfig(confType); err != nil {
			return err
		}

		/*创建主账号*/
		fmt.Println("创建超级管理员开始...")
		if err := loadGlobalLogger(); err != nil {
			return err
		}
		if err := app.InitAllApp(); err != nil {
			return err
		}
		// 创建一个主账号
		us := app.GetInternalApp(user.AppName).(user.Service)
		createUserReq := user.NewCreateUserRequest()
		createUserReq.CreateBy = user.CREATE_BY_ADMIN
		createUserReq.Description = "超级管理员"
		createUserReq.Domain = domain.DEFAULT_DOMAIN
		createUserReq.Username = "admin"
		createUserReq.Password = "123456"
		createUserReq.Type = user.TYPE_SUPPER
		user, err := us.CreateUser(context.Background(), createUserReq)
		if err != nil {
			return err
		}
		fmt.Println("新创建的超级管理员用户 ", user)

		return nil
	},
}

func init() {

	RootCmd.AddCommand(initCmd)
}

/*
Usage:
➜  mcenter git:(master) ✗ make init
创建超级管理员开始...
2022-10-11T18:17:32.719+0800    INFO    [INIT]  cmd/start.go:153        log level: debug
新创建的超级管理员用户  id:"cd2k5f6vvhflqrvbl01g" create_at:1665483452822 spec:{type:SUPPER domain:"default" username:"admin" description:"超级管理员"} profile:{} status:{}

*/
