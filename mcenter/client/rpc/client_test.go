package rpc_test

import (
	"context"
	"gitee.com/zhoushuyu/devcloud/mcenter/apps/endpoint"
	"gitee.com/zhoushuyu/devcloud/mcenter/apps/token"
	"gitee.com/zhoushuyu/devcloud/mcenter/client/rpc"
	"testing"
)

//func TestBookQuery(t *testing.T) {
//	should := assert.New(t)
//
//	conf := client.NewDefaultConfig()
//	// 设置GRPC服务地址
//	// conf.SetAddress("127.0.0.1:8050")
//	// 携带认证信息
//	// conf.SetClientCredentials("secret_id", "secret_key")
//	c, err := client.NewClient(conf)
//	if should.NoError(err) {
//		resp, err := c.Book().QueryBook(
//			context.Background(),
//			book.NewQueryBookRequest(),
//		)
//		should.NoError(err)
//		fmt.Println(resp.Items)
//	}
//}

// 验证token的测试用例
func TestValidateToken(t *testing.T) {
	conf := rpc.NewConfig("192.168.2.2:18050", "twdMMr7PSXQcd8STliv5vz5c", "ug45PUdwSbFxh10rypF4N4S9PBHRvj6J")
	c, err := rpc.NewClient(conf)
	//c, err := rpc.NewClient("192.168.2.2:18050")
	if err != nil {
		t.Fatal(err)
	}
	tk, err := c.Token().ValidateToken(context.Background(), token.NewValidateTokenRequest("avkMvX4YhTURMQolYryr7Bvu"))
	if err != nil {
		t.Fatal(err)
	}
	t.Log(tk)
}

func TestEndpointRegistry(t *testing.T) {
	conf := rpc.NewConfig("192.168.2.2:18050", "twdMMr7PSXQcd8STliv5vz5c", "ug45PUdwSbFxh10rypF4N4S9PBHRvj6J")
	c, err := rpc.NewClient(conf)
	if err != nil {
		t.Fatal(err)
	}

	req := endpoint.NewRegistryRequest("0.0.1", []*endpoint.Entry{
		{
			FunctionName:     "test",
			Path:             "POST./maudit/api/v1/books",
			Method:           "POST",
			Resource:         "Book",
			AuthEnable:       true,
			PermissionEnable: true,
			Labels:           map[string]string{"action": "create"},
		},
	})
	req.ClientId = conf.ClientID
	req.ClientSecret = conf.ClientSecret
	resp, err := c.Endpoint().RegistryEndpoint(context.Background(), req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(resp)
}
