package auth

import (
	"context"
	"fmt"
	"gitee.com/zhoushuyu/devcloud/mcenter/apps/service"
	"gitee.com/zhoushuyu/devcloud/mcenter/client/rpc"
	"github.com/infraboard/mcube/logger"
	"github.com/infraboard/mcube/logger/zap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
)

// grpc认证拦截中间件

// GrpcAuthUnaryServerInterceptor returns a new unary server interceptor for auth.
// 需要将GrpcAuthUnaryServerInterceptor() 添加到protocol/grpc.go 的中间件中
func GrpcAuthUnaryServerInterceptor() grpc.UnaryServerInterceptor {
	// 替换为RPC客户端。思考：如何获取RPC客户端？
	// 方式一： 直接传入RPC客户端
	// 方式二：使用全局变量。把rpc客户端 做一个全局变量, 提供一个Load加载函数, 加载完成后，直接通过rpc.()返回获取该全局变量(rpc客户端实例)
	return newGrpcAuther(rpc.C().Service()).Auth
}

func newGrpcAuther(svr service.RPCClient) *grpcAuther {
	return &grpcAuther{
		log:     zap.L().Named("Grpc Auther"),
		service: svr, // RPC客户端
		//service: nil, // RPC客户端
		/*
			保存信息： went wrong runtime error: invalid memory address or nil pointer dereference
			定为到该文件的 _, err := g.service.ValidateCredential(context.Background(), vsReq)
		*/
	}
}

type grpcAuther struct {
	log     logger.Logger
	service service.RPCClient
}

func (g *grpcAuther) Auth(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (resp interface{}, err error) {
	g.log.Infof("进入mcenter/protocol/auth/grpc.go 的Auth函数")

	// 从context中获取认证信息
	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return nil, fmt.Errorf("ctx is not an grpc incoming context")
	}

	clientId, clientSecret := g.GetClientCredentialsFromMeta(md)
	if err := g.validateServiceCredential(clientId, clientSecret); err != nil {
		return nil, err
	}

	resp, err = handler(ctx, req)
	g.log.Infof("离开mcenter/protocol/auth/grpc.go 的Auth函数")
	return resp, err
}

// 定义拿到clientid和client secret的函数
func (g *grpcAuther) GetClientCredentialsFromMeta(md metadata.MD) (clientId, clientSecret string) {
	cids := md.Get(service.ClientHeaderKey)
	sids := md.Get(service.ClientSecretKey)
	if len(cids) > 0 {
		clientId = cids[0]
	}
	if len(sids) > 0 {
		clientSecret = sids[0]
	}
	return
}

func (g *grpcAuther) validateServiceCredential(clientId, clientSecret string) error {
	if clientId == "" && clientSecret == "" {
		return status.Errorf(codes.Unauthenticated, "client_id or client_secret is \"\"")
	}

	vsReq := service.NewValidateCredentialRequest(clientId, clientSecret)
	_, err := g.service.ValidateCredential(context.Background(), vsReq)
	if err != nil {
		return status.Errorf(codes.Unauthenticated, "service auth error, %s", err)
	}

	return nil
}
