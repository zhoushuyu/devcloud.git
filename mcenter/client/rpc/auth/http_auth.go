package auth

import (
	"fmt"
	"gitee.com/zhoushuyu/devcloud/mcenter/apps/permission"
	"gitee.com/zhoushuyu/devcloud/mcenter/apps/token"
	"gitee.com/zhoushuyu/devcloud/mcenter/client/rpc"
	"github.com/emicklei/go-restful/v3"
	"github.com/infraboard/mcube/http/label"
	"github.com/infraboard/mcube/http/response"
	"github.com/infraboard/mcube/logger"
	"github.com/infraboard/mcube/logger/zap"
)

// 给服务端提供的restful鉴权中间件 包括认证和鉴权

/*
在protocol/http.go中NewHTTPService()这个函数中，需要实现r.Filter(cors.Filter) 函数入参的类型，这个类型就是 FilterFunction

// FilterFunction definitions must call ProcessFilter on the FilterChain to pass on the control and eventually call the RouteFunction
type FilterFunction func(*Request, *Response, *FilterChain)
*/

// 做一个对象实现 FilterFunction 这个类型的函数

type httpAuther struct {
	log logger.Logger
	// 基于RPC客户端进行封装
	client *rpc.ClientSet
}

// 给服务端提供的RESTful接口的 认证与鉴权中间件
func NewHttpAuther() (*httpAuther, error) {
	return &httpAuther{
		log:    zap.L().Named("auth.http"),
		client: rpc.C(), // 从全局变量中获取
	}, nil
}

// 是否开启权限的控制，交给中间件使用方去决定
func (a *httpAuther) GetRestfulAuthFunc(req *restful.Request, resp *restful.Response, next *restful.FilterChain) {
	// 请求拦截
	router := req.SelectedRoute()
	meta := router.Metadata()
	a.log.Debugf("route meta是：%s", meta)

	isAuth, ok := meta[label.Auth]
	if ok && isAuth.(bool) {
		// 获取用户token
		ak := token.GetTokenFromHTTPHeader(req.Request)

		// 调用GRPC 校验用户TOKEN合法性
		tk, err := a.client.Token().ValidateToken(req.Request.Context(), token.NewValidateTokenRequest(ak))
		if err != nil {
			response.Failed(resp.ResponseWriter, err)
			return
		}

		// 是否需要返回用户的用户信息，token本身的信息
		a.log.Debugf("token的信息是: %s", tk)
		req.SetAttribute("tk", tk) // 将token信息写入上下文

		// 判断用户权限
		isPerm, ok := meta[label.Permission]
		if ok && isPerm.(bool) { // 在字典中拿到了值，且value是bool类型
			ci, err := a.client.ClientInfo(req.Request.Context())
			if err != nil {
				response.Failed(resp.ResponseWriter, err)
				return
			}

			// 调用鉴权接口
			check := permission.NewCheckPermissionRequest()
			check.Domain = tk.Domain
			check.Namespace = tk.Namespace
			check.Username = tk.Username
			check.ServiceId = ci.Id
			check.Path = fmt.Sprintf("%s.%s", router.Method(), router.Path())
			perm, err := a.client.Permission().CheckPermission(req.Request.Context(), check)
			if err != nil {
				response.Failed(resp.ResponseWriter, err)
				return
			}
			a.log.Debug("permission check pass. ", perm)
		}
	}

	next.ProcessFilter(req, resp)
}
