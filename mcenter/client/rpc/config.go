package rpc

// NewDefaultConfig todo
//func NewDefaultConfig() *kc.Config {
//	return kc.NewDefaultConfig()
//}

func NewConfig(address, clientId, clientSecret string) *Config {
	return &Config{
		Address:      address,
		ClientID:     clientId,
		ClientSecret: clientSecret,
	}
}

type Config struct {
	Address      string `json:"address" toml:"address"`             // 服务端地址
	ClientID     string `json:"client_id" toml:"client_id"`         // 服务端ClientID
	ClientSecret string `json:"client_secret" toml:"client_secret"` // 服务端ClientSecret
}

func (c *Config) Credentials() *Authentication {
	return NewAuthentication(c.ClientID, c.ClientSecret)
}
