package rpc

import (
	"context"
	"gitee.com/zhoushuyu/devcloud/mcenter/apps/endpoint"
	"gitee.com/zhoushuyu/devcloud/mcenter/apps/permission"
	"gitee.com/zhoushuyu/devcloud/mcenter/apps/service"
	"gitee.com/zhoushuyu/devcloud/mcenter/apps/token"
	"github.com/infraboard/mcube/logger"
	"github.com/infraboard/mcube/logger/zap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"sync"
)

var (
	client *ClientSet
)

// SetGlobal todo
func SetGlobal(cli *ClientSet) {
	client = cli
}

// C Global
func C() *ClientSet {
	if client == nil {
		panic("load rpc config first")
	}
	return client
}

// NewClient todo
//func NewClient(address string) (*ClientSet, error) {
func NewClient(conf *Config) (*ClientSet, error) {
	zap.DevelopmentSetup()
	log := zap.L()

	conn, err := grpc.Dial(
		conf.Address,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
		//grpc.WithPerRPCCredentials(conf.Authentication),
		grpc.WithPerRPCCredentials(conf.Credentials()),
	)
	if err != nil {
		return nil, err
	}

	return &ClientSet{
		conn: conn,
		log:  log,
		conf: conf,
	}, nil
}

// Client 客户端
type ClientSet struct {
	conn *grpc.ClientConn
	log  logger.Logger
	conf *Config
	svr  *service.Service
	lock sync.Mutex
}

// 令牌服务的SDK
func (c *ClientSet) Token() token.RPCClient {
	return token.NewRPCClient(c.conn)
}

// Book服务的SDK
func (c *ClientSet) Endpoint() endpoint.RPCClient {
	return endpoint.NewRPCClient(c.conn)
}

func (c *ClientSet) Permission() permission.RPCClient {
	return permission.NewRPCClient(c.conn)
}

// 返回客户端服务信息
func (c *ClientSet) ClientInfo(ctx context.Context) (*service.Service, error) {
	c.lock.Lock()
	defer c.lock.Unlock()
	if c.svr != nil {
		return c.svr, nil
	}
	req := service.NewValidateCredentialRequest(c.conf.ClientID, c.conf.ClientSecret)
	svc, err := c.Service().ValidateCredential(ctx, req)
	if err != nil {
		return nil, err
	}
	c.svr = svc
	return c.svr, nil
}

// Service() service 内部服务的SDK

func (c *ClientSet) Service() service.RPCClient {
	return service.NewRPCClient(c.conn)
}

func (c *ClientSet) Config() Config {
	return *c.conf
}
