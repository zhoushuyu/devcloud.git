package rest_test

import (
	"context"
	"gitee.com/zhoushuyu/devcloud/mcenter/apps/token"
	"gitee.com/zhoushuyu/devcloud/mcenter/client/rest"
	"testing"
)

var (
	c   *rest.ClientSet
	ctx = context.Background()
)

func init() {
	err := rest.LoadClientFromEnv() // env调试成功后 改成这个 rest.LoadClientFromToml()
	if err != nil {
		panic(err)
	}
	c = rest.C()
}

func TestValidateToken(t *testing.T) {
	req := token.NewValidateTokenRequest("C8jZdXTjBpuGGq6aiO1C5bWw")
	tk, err := c.Token().ValidateToken(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log("哈哈", tk)
}
