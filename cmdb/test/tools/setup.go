package tools

import (
	"gitee.com/zhoushuyu/devcloud/cmdb/conf"
	"github.com/infraboard/mcube/app"
	"github.com/infraboard/mcube/logger/zap"
)

func DevelopmentSetup() {
	zap.DevelopmentSetup()

	err := conf.LoadConfigFromToml("/Users/zhoushuyu/GoProjects/Go8/DEVCLOUD/cmdb/etc/config.toml")
	if err != nil {
		panic(err)
	}

	// 初始化全局app
	if err = app.InitAllApp(); err != nil {
		panic(err)
	}
}
