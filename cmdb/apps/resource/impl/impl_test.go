package impl_test

import (
	"context"
	"gitee.com/zhoushuyu/devcloud/cmdb/apps/resource"
	"gitee.com/zhoushuyu/devcloud/cmdb/test/tools"
	"github.com/infraboard/mcube/app"
	"testing"
	"time"
)

var (
	impl resource.Service
)

func TestSaveResource(t *testing.T) {
	res := &resource.Resource{
		Meta: &resource.Meta{
			ResourceId: "instance_01",
			Domain:     "default",
			Namespace:  "default",
			Env:        "测试环境",
			SyncAt:     time.Now().Unix(),
		},
		Spec: &resource.Spec{
			Vendor:       resource.VENDOR_ALIYUN,
			ResourceType: resource.TYPE_HOST,
			Name:         "test01",
		},
		Cost: &resource.Cost{
			RealCost: 80000,
		},
		Status: &resource.Status{
			Phase: "Running",
		},
		Tags: []*resource.Tag{
			{Key: "app", Value: "app_01", Describe: "应用01"},
			{Key: "app", Value: "app_02", Describe: "应用02"},
		},
	}
	_, err := impl.SaveResource(context.Background(), res)
	if err != nil {
		t.Fatal(err)
	}
}

func TestQueryResource(t *testing.T) {
	req := resource.NewQueryResourceRequest()
	req.ResourceIds = []string{"instance_01", "instance_02"}
	resp, err := impl.QueryResource(context.Background(), req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(resp)
}

func init() {
	tools.DevelopmentSetup()

	impl = app.GetInternalApp(resource.AppName).(resource.Service)
}
