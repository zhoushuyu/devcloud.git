package impl

import (
	"gitee.com/zhoushuyu/devcloud/cmdb/apps/resource"
	"gitee.com/zhoushuyu/devcloud/cmdb/conf"
	"github.com/infraboard/mcube/app"
	"github.com/infraboard/mcube/logger"
	"github.com/infraboard/mcube/logger/zap"
	"google.golang.org/grpc"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

var (
	// Service 服务实例
	svr = &service{}
)

type service struct {
	db *gorm.DB

	log logger.Logger
	resource.UnimplementedRPCServer
}

func (s *service) Config() error {
	db, err := conf.C().MySQL.ORM()
	if err != nil {
		return err
	}

	if conf.C().Log.Level == "debug" {
		db = db.Debug()
	}

	db = db.Clauses(clause.OnConflict{
		UpdateAll: true,
	})

	s.db = db

	s.log = zap.L().Named(s.Name())
	return nil
}

func (s *service) Name() string {
	return resource.AppName
}

func (s *service) Registry(server *grpc.Server) {
	resource.RegisterRPCServer(server, svr)
}

func init() {
	app.RegistryGrpcApp(svr)
	app.RegistryInternalApp(svr)
}
