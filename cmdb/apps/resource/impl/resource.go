package impl

import (
	"context"
	"gitee.com/zhoushuyu/devcloud/cmdb/apps/resource"
)

func (s *service) SaveResource(ctx context.Context, res *resource.Resource) (
	*resource.Resource, error) {
	// 比如检查
	if err := res.Validate(); err != nil {
		return nil, err
	}

	// 入库
	err := s.save(ctx, res)
	if err != nil {
		return nil, err
	}
	return res, nil
}

func (s *service) QueryResource(ctx context.Context, req *resource.QueryResourceRequest) (
	*resource.ResourceSet, error) {
	// 首先查询Meta表
	// LEFT JOIN
	//err := s.db.Table("resource_meta m").Debug().
	//	Joins("LEFT JOIN resource_spec s ON s.resource_id = m.resource_id").
	//	Joins("LEFT JOIN resource_cost c ON c.resource_id = m.resource_id").
	//	Joins("LEFT JOIN resource_status t ON t.resource_id = m.resource_id").
	//	Scan(&temp.Items).Error

	//query := s.db.Table("resource_meta m").Debug().   这一行在mac上执行没有问题。
	query := s.db.WithContext(ctx).Table("resource_meta m").Debug().
		Joins("LEFT JOIN resource_spec s ON s.resource_id = m.resource_id").
		Joins("LEFT JOIN resource_cost c ON c.resource_id = m.resource_id").
		Joins("LEFT JOIN resource_status t ON t.resource_id = m.resource_id")

	if req.Domain != "" {
		query = query.Where("m.domain = ?", req.Domain)
	}
	if req.Namespace != "" {
		query = query.Where("m.namespace = ?", req.Namespace)
	}
	if req.Env != "" {
		query = query.Where("m.env = ?", req.Env)
	}
	if req.Status != "" {
		query = query.Where("m.status = ?", req.Status)
	}
	if len(req.ResourceIds) > 0 {
		query = query.Where("m.resource_id IN (?)", req.ResourceIds)
	}

	temp := NewResourceSet()

	// 统计查询出的数量
	var total int64 = 0
	err := query.Count(&total).Error
	if err != nil {
		return nil, err
	}

	err = query.Offset(int(req.Page.ComputeOffset())).Limit(int(req.Page.PageSize)).Scan(&temp.Items).Error
	if err != nil {
		return nil, err
	}

	set := temp.ResourceSet()
	set.Total = total

	return set, nil
}
