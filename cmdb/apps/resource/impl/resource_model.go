package impl

import "gitee.com/zhoushuyu/devcloud/cmdb/apps/resource"

func NewResource() *Resource {
	return &Resource{
		Meta:   &resource.Meta{},
		Spec:   NewSpec(),
		Cost:   NewCost(),
		Status: NewStatus(),
	}
}

func NewResourceSet() *ResourceSet {
	return &ResourceSet{
		Items: []*Resource{},
	}
}

type ResourceSet struct {
	Total int64
	Items []*Resource
}

func (s *ResourceSet) ResourceSet() *resource.ResourceSet {
	set := resource.NewResourceSet()
	set.Total = s.Total
	for i := range s.Items {
		item := s.Items[i]
		set.Add(&resource.Resource{
			Meta:   item.Meta,
			Spec:   item.Spec.Spec,
			Cost:   item.Cost.Cost,
			Status: item.Status.Status,
		})
	}

	return set
}

type Resource struct {
	*resource.Meta
	*Spec
	*Cost
	*Status
}

type ResourceId struct {
	ResourceId string `json:"resource_id"`
}

func NewSpec() *Spec {
	return &Spec{
		ResourceId: &ResourceId{},
		Spec:       &resource.Spec{},
	}
}

type Spec struct {
	*ResourceId
	*resource.Spec
}

func (s *Spec) TableName() string {
	return "resource_spec"
}

func NewCost() *Cost {
	return &Cost{
		ResourceId: &ResourceId{},
		Cost:       &resource.Cost{},
	}
}

type Cost struct {
	*ResourceId
	*resource.Cost
}

func (m *Cost) TableName() string {
	return "resource_cost"
}

func NewStatus() *Status {
	return &Status{
		ResourceId: &ResourceId{},
		Status:     &resource.Status{},
	}
}

type Status struct {
	*ResourceId
	*resource.Status
}

func (m *Status) TableName() string {
	return "resource_status"
}

type Tag struct {
	*ResourceId
	*resource.Tag
}

func (m *Tag) TableName() string {
	return "resource_tag"
}
