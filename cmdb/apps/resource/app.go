package resource

import (
	"fmt"
	"github.com/go-playground/validator/v10"
	"github.com/infraboard/mcube/http/request"
	"net/http"
)

const (
	AppName = "resource"
)

var (
	validate = validator.New()
)

type Service interface {
	RPCServer
}

func (m *Meta) TableName() string {
	return "resource_meta"
}

func (r *Resource) Validate() error {
	validateStructs := []any{r.Meta, r.Spec, r.Cost}
	for _, v := range validateStructs {
		err := validate.Struct(v)
		if err != nil {
			return err
		}
	}

	return nil
}

func NewQueryResourceRequest() *QueryResourceRequest {
	return &QueryResourceRequest{}
}

func NewDefaultResource(rt TYPE) *Resource {
	return &Resource{
		Meta: &Meta{},
		Spec: &Spec{
			ResourceType: rt,
		},
		Cost:   &Cost{},
		Status: &Status{},
		Tags:   []*Tag{},
	}
}

func NewResourceSet() *ResourceSet {
	return &ResourceSet{
		Items: []*Resource{},
	}
}

func (s *ResourceSet) Add(item *Resource) {
	s.Items = append(s.Items, item)
}

func NewQueryResourceRequestFromHttp(r *http.Request) *QueryResourceRequest {
	res := NewQueryResourceRequest()

	qs := r.URL.Query() // 得到请求参数
	fmt.Println("app.go中NewQueryResourceRequestFromHttp函数，qs的值是：", qs)
	res.Domain = qs.Get("domain")
	res.Namespace = qs.Get("namespace")
	res.Env = qs.Get("env")
	res.Page = request.NewPageRequestFromHTTP(r) // 拿到page参数

	return res
}
