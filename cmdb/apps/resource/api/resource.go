package api

import (
	"gitee.com/zhoushuyu/devcloud/cmdb/apps/resource"
	"github.com/emicklei/go-restful/v3"
	"github.com/infraboard/mcube/http/response"
)

func (h *handler) QueryResource(r *restful.Request, w *restful.Response) {
	req := resource.NewQueryResourceRequestFromHttp(r.Request)

	set, err := h.service.QueryResource(r.Request.Context(), req)
	if err != nil {
		response.Failed(w.ResponseWriter, err)
		return
	}

	response.Success(w.ResponseWriter, set)
}
