package api

import (
	"gitee.com/zhoushuyu/devcloud/cmdb/apps/resource"
	restfulspec "github.com/emicklei/go-restful-openapi"
	"github.com/emicklei/go-restful/v3"
	"github.com/infraboard/mcube/app"
	"github.com/infraboard/mcube/http/label"
	"github.com/infraboard/mcube/logger"
	"github.com/infraboard/mcube/logger/zap"
)

var (
	h1 = &handler{}
)

type handler struct {
	service resource.Service
	log     logger.Logger
}

func (h *handler) Config() error {
	h.log = zap.L().Named(resource.AppName)
	h.service = app.GetGrpcApp(resource.AppName).(resource.Service)

	return nil
}

func (h *handler) Name() string {
	return resource.AppName
}

func (h *handler) Version() string {
	return "v1"
}

func (h *handler) Registry(ws *restful.WebService) {
	tags := []string{"资源管理"}

	ws.Route(ws.GET("/").To(h.QueryResource).
		Metadata(label.Auth, true).
		Doc("查询资源").
		Metadata(restfulspec.KeyOpenAPITags, tags))
}

func init() {
	//app.RegistryInternalApp(h)  如果这里也注册了InternalApp就会报错。报错如下
	/*
		panic: internal app resource has registed

		goroutine 1 [running]:
		github.com/infraboard/mcube/app.RegistryInternalApp({0x19c3de0, 0x1e923e0})
		        /Users/zhoushuyu/go/pkg/mod/github.com/infraboard/mcube@v1.9.0/app/internal.go:20 +0x10d
		gitee.com/zhoushuyu/devcloud/cmdb/apps/resource/impl.init.0()
		        /Users/zhoushuyu/GoProjects/Go8/DEVCLOUD/cmdb/apps/resource/impl/impl.go:52 +0x3a    这一行是：app.RegistryInternalApp(svr) 就是注册重复了
		exit status 2

	*/
	app.RegistryRESTfulApp(h1)
}
