package rpc_test

import (
	"context"
	"gitee.com/zhoushuyu/devcloud/maudit/apps/operate"
	"gitee.com/zhoushuyu/devcloud/maudit/client/rpc"
	"testing"
)

var client *rpc.ClientSet

func TestSaveOperateLog(t *testing.T) {
	err := rpc.LoadClient()
	if err != nil {
		panic(err)
	}
	req := operate.NewOperateLog()
	req.Domain = "default"
	req.Namespace = "default"
	req.Username = "userb"
	req.ResourceType = "ecs"
	req.Action = "created"

	client = rpc.C()
	tk, err := client.Operate().SaveOperateLog(context.Background(), req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(tk)
}

func TestQueryOperateLog(t *testing.T) {
	err := rpc.LoadClient()
	if err != nil {
		panic(err)
	}

	req := operate.NewQueryOperateLogRequest()
	client = rpc.C()
	tk, err := client.Operate().QueryOperateLog(context.Background(), req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(tk)
}