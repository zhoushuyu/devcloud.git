package middleware

import (
	"fmt"
	"gitee.com/zhoushuyu/devcloud/maudit/apps/operate"
	"gitee.com/zhoushuyu/devcloud/maudit/client/rpc"
	"gitee.com/zhoushuyu/devcloud/mcenter/apps/token"
	"github.com/emicklei/go-restful/v3"
	"github.com/infraboard/mcube/http/label"
	"github.com/infraboard/mcube/logger"
	"github.com/infraboard/mcube/logger/zap"
	"time"
)

// 给服务端提供的RESTful 接口，认证与授权中间件
func NewHttpAuditor() (*HttpAudit, error) {
	return &HttpAudit{
		log:    zap.L().Named("audit.http"),
		client: rpc.C(),
	}, nil
}

type HttpAudit struct {
	log    logger.Logger
	client *rpc.ClientSet
}

// 是否开启权限的控制，交给中间件使用方去决定
func (a *HttpAudit) GoRestfulAuditFunc(req *restful.Request, resp *restful.Response, next *restful.FilterChain) {
	// 请求拦截
	route := req.SelectedRoute()
	meta := route.Metadata()
	a.log.Debugf("router meta: %s ", meta)

	obj := req.Attribute("tk")

	var record *operate.OperateLog
	isAudit, ok := meta[label.Audit]
	// 有认证标签，且开启了认证
	if ok && obj != nil && isAudit.(bool) {
		tk := obj.(token.Token)
		record := operate.NewOperateLog()
		record.Domain = tk.Domain
		record.Namespace = tk.Namespace
		record.Username = tk.Username
		record.UserAgent = req.Request.UserAgent()
		record.Time = time.Now().UnixMilli()
		record.Action = route.Operation()
		record.Detail = fmt.Sprintf("%s.%s", route.Method(), route.Path())
		record.Request = req.Request.URL.String()
		a.client.Operate().SaveOperateLog(req.Request.Context(), record)
	}

	next.ProcessFilter(req, resp)

	// 记录响应日志
	// 有审计标签，且开启了审计
	if ok && obj != nil && isAudit.(bool) && record != nil {
		record.Response = ""
	}

	if record != nil {
		a.log.Debugf("save audit log")
		a.client.Operate().SaveOperateLog(req.Request.Context(), record)
	}
}
