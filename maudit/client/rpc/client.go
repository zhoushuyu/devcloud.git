package rpc

import (
	"fmt"
	"gitee.com/zhoushuyu/devcloud/maudit/apps/operate"
	"gitee.com/zhoushuyu/devcloud/maudit/client/rpc/resolver"
	"gitee.com/zhoushuyu/devcloud/mcenter/client/rpc"
	"github.com/infraboard/mcube/logger"
	"github.com/infraboard/mcube/logger/zap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

//var (
//	client *ClientSet
//)
//
//// SetGlobal todo
//func SetGlobal(cli *ClientSet) {
//	client = cli
//}
//
//// C Global
//func C() *ClientSet {
//	if client == nil {
//		panic("load rpc config first")
//	}
//	return client
//}

func NewClient(conf *rpc.Config) (*ClientSet, error) {
	zap.DevelopmentSetup()
	log := zap.L()

	//resolver.SetAddr("maudit", []string{"192.168.2.2:19010", "192.168.2.2:19020"})  // 启动多个maudit服务，做客户端负载均衡
	resolver.SetAddr("maudit", []string{"192.168.2.2:19010"})
	conn, err := grpc.Dial(
		//conf.Address,
		fmt.Sprintf("%s:///maudit", resolver.StaticScheme),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
		grpc.WithPerRPCCredentials(conf.Credentials()),
		// 配置客户端负载均衡策略
		grpc.WithDefaultServiceConfig(`{"loadBalancingConfig":[{"round_robin":{}}]}`),
	)
	if err != nil {
		return nil, err
	}

	return &ClientSet{
		conn: conn,
		log:  log,
	}, nil
}

type ClientSet struct {
	conn *grpc.ClientConn
	log  logger.Logger
}

func (c *ClientSet) Operate() operate.RPCClient {
	return operate.NewRPCClient(c.conn)
}

/*
   Invoke(ctx context.Context, method string, args interface{}, reply interface{}, opts ...CallOption) error
   NewStream(ctx context.Context, desc *StreamDesc, method string, opts ...CallOption) (ClientStream, error)
*/
