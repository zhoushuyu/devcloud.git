/*
需求：如果 maudit进行横向扩展
方案：
	1、客户端负载均衡
		客户端需要支持多个，这个grpc本身就支持，需要再写一个resolver做解析
		内部服务一般走客户端负载均衡
	2、服务端负载均衡
		可以通过nginx实现

resolver 是一个地址解析器。有 service name ---> service address列表 的映射。类似于DNS
1、静态地址列表

2、动态地址列表
*/

package resolver

import "google.golang.org/grpc/resolver"

const (
	// static:///maudit
	StaticScheme = "static" // 这个就是 scheme
)

var (
	addrsStore = map[string][]string{}
)

func SetAddr(serviceName string, addrs []string) {
	addrsStore[serviceName] = addrs
}

// Following is an example name resolver. It includes a
// ResolverBuilder(https://godoc.org/google.golang.org/grpc/resolver#Builder)
// and a Resolver(https://godoc.org/google.golang.org/grpc/resolver#Resolver).
//
// A ResolverBuilder is registered for a scheme (in this example, "example" is
// the scheme). When a ClientConn is created for this scheme, the
// ResolverBuilder will be picked to build a Resolver. Note that a new Resolver
// is built for each ClientConn. The Resolver will watch the updates for the
// target, and send updates to the ClientConn.

// exampleResolverBuilder is a
// ResolverBuilder(https://godoc.org/google.golang.org/grpc/resolver#Builder).
type exampleResolverBuilder struct{}

func (*exampleResolverBuilder) Build(target resolver.Target, cc resolver.ClientConn, opts resolver.BuildOptions) (resolver.Resolver, error) {
	r := &exampleResolver{
		target:     target,
		cc:         cc,
		addrsStore: addrsStore,
	}
	r.start()
	return r, nil
}
func (*exampleResolverBuilder) Scheme() string { return StaticScheme }

// exampleResolver is a
// Resolver(https://godoc.org/google.golang.org/grpc/resolver#Resolver).
type exampleResolver struct {
	target     resolver.Target
	cc         resolver.ClientConn
	addrsStore map[string][]string
}

func (r *exampleResolver) start() {
	addrStrs := r.addrsStore[r.target.Endpoint]
	addrs := make([]resolver.Address, len(addrStrs))

	for i, s := range addrStrs {
		addrs[i] = resolver.Address{Addr: s}
	}

	r.cc.UpdateState(resolver.State{Addresses: addrs})
}
func (e *exampleResolver) ResolveNow(o resolver.ResolveNowOptions) {
	e.start()
}
func (*exampleResolver) Close() {}

func init() {
	// Register the example ResolverBuilder. This is usually done in a package's
	// init() function.
	resolver.Register(&exampleResolverBuilder{})
}
