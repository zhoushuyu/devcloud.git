/*
生产者的Publisher结构体的Pub方法，kafka_test.go中进行调用，或者在其他服务中进行调用，用于向数据总线中发送数据。
*/

package bus

import (
	"context"
	"encoding/json"
	"gitee.com/zhoushuyu/devcloud/maudit/apps/operate"
	"github.com/infraboard/mcube/app"
	"github.com/infraboard/mcube/logger"
	"github.com/infraboard/mcube/logger/zap"
	"github.com/segmentio/kafka-go"
)

const (
	OPERATE_LOG_TOPIC = "log.operate"
)

// Publisher的构造函数，需要传入kafka集群的地址
func NewPublisher(addr []string) *Publisher {
	w := kafka.NewWriter(kafka.WriterConfig{
		Brokers: addr,
		Topic:   OPERATE_LOG_TOPIC,
	})

	return &Publisher{
		log:      zap.L().Named("bus.operate"),
		producer: w,
	}
}

// 定义生产者/发布者

type Publisher struct {
	log      logger.Logger
	producer *kafka.Writer
}

// 定义Publisher结构体方法 Pub ，用于向数据总线上发送数据

func (b *Publisher) Pub(ctx context.Context, record *operate.OperateLog) error {
	// 将日志转换为kafka.Message中Value字段需要的byte
	payload, err := json.Marshal(record)
	if err != nil {
		return err
	}

	// 构造message
	m := kafka.Message{
		Value: payload,
	}

	return b.producer.WriteMessages(ctx, m)
}

// Subscriber，需要传入kafka集群的地址
func NewSubscriber(addr []string) *Subscriber {
	r := kafka.NewReader(kafka.ReaderConfig{
		Brokers: addr,
		GroupID: "consumer-group-id",
		Topic:   OPERATE_LOG_TOPIC,
	})

	return &Subscriber{
		log:      zap.L().Named("bus.operate"),
		consumer: r,
		operate:  app.GetInternalApp(operate.AppName).(operate.Service),
	}
}

// 定义消费者
type Subscriber struct {
	log      logger.Logger
	consumer *kafka.Reader
	operate  operate.Service
}

// 定义Subscriber结构体方法 Sub ，用于从数据总线上读取数据

func (b *Subscriber) Sub(ctx context.Context) {
	b.log.Infof("开始从消息总线上订阅消息...")

	for {
		// 获取数据
		m, err := b.consumer.ReadMessage(ctx)
		if err != nil {
			b.log.Errorf("读取消息出现异常，%s ", err.Error())
			return
		}

		b.log.Debugf("message所在的topic/partition/offset是：%v/%v/%v,消费者消费的消息：%s -> %s", m.Topic, m.Partition, m.Offset, string(m.Key), string(m.Value))

		// 进行反序列化
		record := operate.NewOperateLog()
		if err := json.Unmarshal(m.Value, record); err != nil {
			b.log.Errorf("序列化消息异常，%s ", err.Error())
			continue
		}

		// 将数据写数据库
		_, err = b.operate.SaveOperateLog(ctx, record)
		if err != nil {
			b.log.Errorf("消息入库异常，%s ", err.Error())
			return
		}

		// 处理完消息后，进行commit，防止在消费者挂了后找不到上次消费到的位置
		if err := b.consumer.CommitMessages(ctx, m); err != nil {
			b.log.Errorf("提交消息异常，%s ", err.Error())
		}

		//m.Offset = m.Offset + 1
	}
}
