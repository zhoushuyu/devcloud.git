package bus_test

import (
	"context"
	"gitee.com/zhoushuyu/devcloud/maudit/apps/operate"
	"gitee.com/zhoushuyu/devcloud/maudit/client/bus"
	"testing"
)

func TestPubOperateLog(t *testing.T) {
	req := operate.NewOperateLog()
	req.Domain = "default"
	req.Namespace = "default"
	req.Username = "zhoushuyu"
	req.ResourceType = "ecs"
	req.Action = "bus test11"
	p := bus.NewPublisher([]string{"192.168.2.45:9092", "192.168.2.46:9092", "192.168.2.47:9092"})
	err := p.Pub(context.Background(), req)
	if err != nil {
		t.Fatal(err)
	}
}
