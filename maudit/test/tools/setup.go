package tools

import (
	"gitee.com/zhoushuyu/devcloud/maudit/conf"
	"github.com/infraboard/mcube/app"
	"github.com/infraboard/mcube/logger/zap"

	// 注册所有服务
	_ "gitee.com/zhoushuyu/devcloud/maudit/apps"
)

func DevelopmentSetup() {
	// 初始化日志模块
	zap.DevelopmentSetup()

	err := conf.LoadConfigFromToml("/Users/zhoushuyu/GoProjects/Go8/DEVCLOUD/maudit/etc/config.toml")
	if err != nil {
		panic(err)
	}

	// 初始化全局app
	if err = app.InitAllApp(); err != nil {
		panic(err)
	}
}
