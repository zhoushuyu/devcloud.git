package impl_test

import (
	"context"
	"gitee.com/zhoushuyu/devcloud/maudit/apps/operate"
	"gitee.com/zhoushuyu/devcloud/maudit/test/tools"
	"github.com/infraboard/mcube/app"
	"testing"
)

var (
	impl operate.Service
	ctx  = context.Background()
)

func TestSaveOperateLog(t *testing.T) {
	req := operate.NewOperateLog()
	req.Domain = "myDomain"
	req.Namespace = "myNamespace"
	req.Username = "myUsername"
	req.ResourceType = "myResourceType"
	req.Action = "myAction"
	resp, err := impl.SaveOperateLog(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(resp)
}

func TestQueryOperateLog(t *testing.T) {
	req := operate.NewQueryOperateLogRequest()
	resp, err := impl.QueryOperateLog(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(resp)
}

func init() {
	tools.DevelopmentSetup()
	impl = app.GetInternalApp(operate.AppName).(operate.Service)
}
