package impl

import (
	"context"
	"gitee.com/zhoushuyu/devcloud/maudit/apps/operate"
)

func (s *service) save(ctx context.Context, ins *operate.OperateLog) error {
	_, err := s.col.InsertOne(ctx, ins)
	if err != nil {
		return err
	}
	return nil
}
