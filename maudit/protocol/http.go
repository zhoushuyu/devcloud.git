package protocol

import (
	"context"
	"fmt"
	"gitee.com/zhoushuyu/devcloud/mcenter/apps/endpoint"
	"gitee.com/zhoushuyu/devcloud/mcenter/client/rpc"
	"gitee.com/zhoushuyu/devcloud/mcenter/client/rpc/auth"
	"gitee.com/zhoushuyu/devcloud/mcenter/client/rpc/tools"
	"net/http"
	"time"

	restfulspec "github.com/emicklei/go-restful-openapi/v2"
	"github.com/emicklei/go-restful/v3"
	"github.com/infraboard/mcube/logger"
	"github.com/infraboard/mcube/logger/zap"

	"github.com/infraboard/mcube/app"

	"gitee.com/zhoushuyu/devcloud/maudit/conf"
	"gitee.com/zhoushuyu/devcloud/maudit/swagger"
)

// NewHTTPService 构建函数
func NewHTTPService() *HTTPService {

	r := restful.DefaultContainer
	// Optionally, you can install the Swagger Service which provides a nice Web UI on your REST API
	// You need to download the Swagger HTML5 assets and change the FilePath location in the config below.
	// Open http://localhost:8080/apidocs/?url=http://localhost:8080/apidocs.json
	// http.Handle("/apidocs/", http.StripPrefix("/apidocs/", http.FileServer(http.Dir("/Users/emicklei/Projects/swagger-ui/dist"))))

	// Optionally, you may need to enable CORS for the UI to work.
	cors := restful.CrossOriginResourceSharing{
		AllowedHeaders: []string{"*"},
		AllowedMethods: []string{"*"},
		CookiesAllowed: false,
		Container:      r}
	r.Filter(cors.Filter)
	// 添加认证中间件
	// auth是使用的 mcenter/client/rpc/auth
	//mconf := rpc.NewConfig("192.168.2.2:18050", "twdMMr7PSXQcd8STliv5vz5c", "ug45PUdwSbFxh10rypF4N4S9PBHRvj6J")
	am, err := auth.NewHttpAuther()
	if err != nil {
		panic(err)
	}
	r.Filter(am.GetRestfulAuthFunc)

	// 获取mcenter的客户端
	//mc, err := rpc.NewClient(mconf)
	//if err != nil {
	//	panic(err)
	//}

	server := &http.Server{
		ReadHeaderTimeout: 60 * time.Second,
		ReadTimeout:       60 * time.Second,
		WriteTimeout:      60 * time.Second,
		IdleTimeout:       60 * time.Second,
		MaxHeaderBytes:    1 << 20, // 1M
		Addr:              conf.C().App.HTTP.Addr(),
		Handler:           r,
	}

	return &HTTPService{
		r:      r,
		server: server,
		l:      zap.L().Named("HTTP Service"),
		c:      conf.C(),
		mc:     rpc.C(),
	}
}

// HTTPService http服务
type HTTPService struct {
	r      *restful.Container
	l      logger.Logger
	c      *conf.Config
	server *http.Server
	mc     *rpc.ClientSet
}

func (s *HTTPService) PathPrefix() string {
	return fmt.Sprintf("/%s/api", s.c.App.Name)
}

// Start 启动服务
func (s *HTTPService) Start() error {
	// 装置子服务路由
	app.LoadRESTfulApp(s.PathPrefix(), s.r)

	// 注册路由条目
	s.RegistryEndpoint(context.Background())

	// API Doc
	config := restfulspec.Config{
		WebServices:                   restful.RegisteredWebServices(), // you control what services are visible
		APIPath:                       "/apidocs.json",
		PostBuildSwaggerObjectHandler: swagger.Docs}
	s.r.Add(restfulspec.NewOpenAPIService(config))
	s.l.Infof("Get the API using http://%s%s", s.c.App.HTTP.Addr(), config.APIPath)

	// 启动 HTTP服务
	s.l.Infof("HTTP服务启动成功, 监听地址: %s", s.server.Addr)
	if err := s.server.ListenAndServe(); err != nil {
		if err == http.ErrServerClosed {
			s.l.Info("service is stopped")
		}
		return fmt.Errorf("start service error, %s", err.Error())
	}
	return nil
}

// Stop 停止server
func (s *HTTPService) Stop() error {
	s.l.Info("start graceful shutdown")
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	// 优雅关闭HTTP服务
	if err := s.server.Shutdown(ctx); err != nil {
		s.l.Errorf("graceful shutdown timeout, force exit")
	}
	return nil
}

// 注册功能列表
func (s *HTTPService) RegistryEndpoint(ctx context.Context) {
	epss := []*endpoint.Entry{}
	wss := s.r.RegisteredWebServices()
	for i := range wss {
		eps := tools.TransferRoutesToEndpoints(wss[i].Routes())
		epss = append(epss, eps...)
	}

	// mcenter的客户端
	req := endpoint.NewRegistryRequest("0.0.1", epss)
	fmt.Println(s.mc.Config().ClientID)
	req.ClientId = s.mc.Config().ClientID
	req.ClientSecret = s.mc.Config().ClientSecret
	resp, err := s.mc.Endpoint().RegistryEndpoint(ctx, req)
	if err != nil {
		s.l.Errorf("registry endpoint error, %s", err)
	} else {
		s.l.Debugf("registy success, %s", resp.Message)
	}

	//epss := []*endpoint.Entry{}
	//wss := s.r.RegisteredWebServices()
	//for _, v := range wss {
	//	routes := v.Routes()
	//	eps := tools.TransferRoutesToEndpoints(routes)
	//	epss = append(epss, eps...)
	//	s.l.Debug(eps)
	//}
	//s.l.Debug(epss)
	//// mcenter客户端
	//req := endpoint.NewRegistryRequest("0.0.1", epss)
	//req.ClientId = s.mc.Config().ClientID
	//req.ClientSecret = s.mc.Config().ClientSecret
	//
	//resp, err := s.mc.Endpoint().RegistryEndpoint(ctx, req)
	//if err != nil {
	//	s.l.Errorf("注册endpoint失败, %s", err)
	//} else {
	//	s.l.Debugf("注册成功，%s", resp.Message)
	//}
}
