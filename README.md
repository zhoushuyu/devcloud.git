# 研发云

## 功能
+ 用户管理：管理登录用户
  + 用户的CRUD

+ 权限管理：管理哪些用户可以使用哪些功能
  + 策略管理：基于角色的策略管理模型
    + 角色：一组功能的集合
      + 功能列表：整个平台有哪些功能(平台所有服务Restful的接口)
        + 功能条目注册：服务将自己的接口(resource:action)，注册到权限模块
    + 用户：用户是什么角色
    + 空间：如何进行资源隔离 （各个业务线只能操作各自的服务）
    + 环境：开发环境 测试环境 生产环境


+ 服务管理：对开发中的服务进行关联
  + 服务创建
    + 系统服务
      + 平台需要进行的基础服务
    + 业务服务
      + 开发具体的商业应用的服务
  + 服务查看

+ cmdb资产管理 对业务开发需要的资源进行关联
  + 主机
  + Rds
  + Bucket
  + Redis

+ 发布平台：继续k8s的微服务部署平台（Deploy/DaemonSet）
  + 进行服务的发布
  + 服务的查询

## 架构

## 微服务开发

+ 用户中心(mcenter)
+ 审计中心(maudit)
+ 资源中心(cmdb)

+ 微服务网关(traefik)